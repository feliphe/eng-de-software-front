System.register(["./controller/LoginController"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var LoginController_1, contr;
    return {
        setters: [
            function (LoginController_1_1) {
                LoginController_1 = LoginController_1_1;
            }
        ],
        execute: function () {
            contr = new LoginController_1.LoginController();
            document
                .querySelector('#formregister')
                .addEventListener('submit', contr.registerUser.bind(contr));
            document
                .querySelector('#formlogin')
                .addEventListener('submit', contr.login.bind(contr));
            document
                .querySelector('#btnRegse')
                .addEventListener('click', contr.showRegisterForm.bind(contr));
            document
                .querySelector('#btnReg')
                .addEventListener('click', contr.showLoginForm.bind(contr));
        }
    };
});
