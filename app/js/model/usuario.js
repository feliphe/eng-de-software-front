System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Usuario;
    return {
        setters: [],
        execute: function () {
            Usuario = class Usuario {
                constructor() {
                    this._email = $('#email-login').val();
                    this._senha = $('#senha-login').val();
                }
                setEmail(value) {
                    this._email = value;
                }
                getEmail() {
                    return this._email;
                }
                setSenha(value) {
                    this._senha = value;
                }
                getSenha() {
                    return this._senha;
                }
            };
            exports_1("Usuario", Usuario);
        }
    };
});
