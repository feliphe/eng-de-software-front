System.register(["../model/Usuario", "../service/LoginService", "../model/UsuarioNewDTO"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Usuario_1, LoginService_1, UsuarioNewDTO_1, LoginController;
    return {
        setters: [
            function (Usuario_1_1) {
                Usuario_1 = Usuario_1_1;
            },
            function (LoginService_1_1) {
                LoginService_1 = LoginService_1_1;
            },
            function (UsuarioNewDTO_1_1) {
                UsuarioNewDTO_1 = UsuarioNewDTO_1_1;
            }
        ],
        execute: function () {
            LoginController = class LoginController {
                constructor() {
                    this.service = new LoginService_1.LoginService();
                }
                registerUser() {
                    event.preventDefault();
                    let user = new UsuarioNewDTO_1.UsuarioNewDTO();
                    let dados = {
                        email: user.getEmail(),
                        senha: user.getSenha(),
                        cpfOuCnpj: user.getCfpOuCnpj()
                    };
                    $.ajax({
                        url: 'http://192.168.25.152:8080/usuarios',
                        type: 'post',
                        dataType: 'json',
                        contentType: 'application/json',
                        success: () => {
                            console.log('sucesso');
                        },
                        data: dados
                    });
                }
                login() {
                    event.preventDefault();
                    let user = new Usuario_1.Usuario();
                    fetch(`http://192.168.25.152:8080/usuarios/email?value=${user.getEmail()}&senha=${user.getSenha()}`)
                        .then(res => this.service.isOk(res))
                        .then(res => res.json())
                        .then(obj => this.service.verify(obj.email, obj.senha))
                        .catch(err => console.log(err));
                }
                showRegisterForm() {
                    document.querySelector('#formregister').classList.remove('unsighted');
                    document.querySelector('#formlogin').classList.add('unsighted');
                }
                showLoginForm() {
                    document.querySelector('#formregister').classList.add('unsighted');
                    document.querySelector('#formlogin').classList.remove('unsighted');
                }
            };
            exports_1("LoginController", LoginController);
        }
    };
});
