export class Usuario {

    private _email: string;
    private _senha: any;

    constructor() {
        this._email = $('#email-login').val() as string;        
        this._senha = $('#senha-login').val() as string;
    }

    setEmail(value: string) {
        this._email = value;
    }
    
    getEmail() : string {
        return this._email;
    }

    setSenha(value: string) {
        this._senha = value;
    }
    
    getSenha() : string {
        return this._senha;
    }
}