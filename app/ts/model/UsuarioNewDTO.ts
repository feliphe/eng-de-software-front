export class UsuarioNewDTO {

    private _email: string;
    private _senha: string;
    private _cpfOuCnpj: string;

    constructor() {
        this._email = $('#email-reg').text();
        this._senha = $('#senha-reg').text();
        this._cpfOuCnpj = $('#cpfOuCnpj').text();
    }
    
    getEmail() : string {
        return this._email;
    }
    
    getSenha() : string {
        return this._email;
    }

    getCfpOuCnpj(): string {
        return this._cpfOuCnpj;
    }
}