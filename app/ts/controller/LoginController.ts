import { Usuario } from '../model/Usuario';
import { LoginService } from '../service/LoginService';
import { UsuarioNewDTO } from '../model/UsuarioNewDTO';

export class LoginController {

    private service: LoginService = new LoginService();

    public registerUser() {
        
        event.preventDefault();

        let user: UsuarioNewDTO = new UsuarioNewDTO();

        let dados = {
            email: user.getEmail(),
            senha: user.getSenha(),
            cpfOuCnpj: user.getCfpOuCnpj()
        }

        $.ajax({
            url: 'http://192.168.25.152:8080/usuarios',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: () => {
                console.log('sucesso');
            },
            data: dados
        });

    }

    public login() {

        event.preventDefault();

        let user: Usuario = new Usuario();
        
        fetch(`http://192.168.25.152:8080/usuarios/email?value=${user.getEmail()}&senha=${user.getSenha()}`)
            .then(res => this.service.isOk(res))
            .then(res => res.json())
            .then(obj => this.service.verify(obj.email, obj.senha))
            .catch(err => console.log(err));
    }

    public showRegisterForm() {
        document.querySelector('#formregister').classList.remove('unsighted');
        document.querySelector('#formlogin').classList.add('unsighted');
    }

    public showLoginForm() {
        document.querySelector('#formregister').classList.add('unsighted');
        document.querySelector('#formlogin').classList.remove('unsighted');
    }

}