import { LoginController } from './controller/LoginController';
import { Usuario } from './model/Usuario';

const contr = new LoginController();

document
    .querySelector('#formregister')
    .addEventListener('submit', contr.registerUser.bind(contr));

document
    .querySelector('#formlogin')
    .addEventListener('submit', contr.login.bind(contr));
    
document
    .querySelector('#btnRegse')
    .addEventListener('click', contr.showRegisterForm.bind(contr));

document
    .querySelector('#btnReg')
    .addEventListener('click', contr.showLoginForm.bind(contr));